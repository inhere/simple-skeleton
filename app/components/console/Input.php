<?php
/**
 * Created by PhpStorm.
 * User: inhere
 * Date: 2016/12/7
 * Time: 19:23
 */

namespace app\components\console;

/**
 * Class Input
 * @package app\components\console
 */
class Input extends \app\components\Input
{
    protected $inputStream = STDIN;

    /**
     * @return string
     */
    public function read()
    {
        return trim(fgets($this->inputStream));
    }
}