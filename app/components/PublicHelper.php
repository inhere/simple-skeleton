<?php
/**
 * Created by PhpStorm.
 * User: inhere
 * Date: 2016/9/27
 * Time: 18:33
 */

namespace app\components;

/**
 * Class PublicHelper
 * @package app\components
 */
class PublicHelper
{
    /**
     * @return int
     */
    public static function requestTime()
    {
        return isset($_SERVER['REQUEST_TIME']) ? $_SERVER['REQUEST_TIME'] : time();
    }

    /**
     * @param bool|true $toInt
     * @return mixed
     */
    public static function requestMicroTime($toInt=true)
    {
        $float = isset($_SERVER['REQUEST_TIME_FLOAT']) ? $_SERVER['REQUEST_TIME_FLOAT'] : microtime(1);

        return $toInt ? $float * 10000 : $float;
    }

    /**
     * @return int
     */
    public static function todayStart()
    {
//        return strtotime(date('Y-m-d 00:00:00'));
        return strtotime('today 00:00:00');
    }

    /**
     * @return int
     */
    public static function todayEnd()
    {
//        return strtotime(date('Y-m-d 23:59:59'));
        return strtotime('today 23:59:59');
    }

    /**
     * @return int
     */
    public static function tomorrowStart()
    {
        return strtotime('+1 day 00:00:00');
    }

    /**
     * @return int
     */
    public static function tomorrowEnd()
    {
        return strtotime('+1 day 23:59:59');
    }

    /**
     * isWeb
     * @return  boolean
     */
    public static function isWeb()
    {
        return in_array(
            PHP_SAPI,
            array(
                'apache',
                'cgi',
                'fast-cgi',
                'cgi-fcgi',
                'fpm-fcgi',
                'srv',
                'cli-server'
            )
        );
    }

    public static function isBuildInServer()
    {
        return PHP_SAPI === 'cli-server';
    }

    /**
     * isCli
     * @return  boolean
     */
    public static function isCli()
    {
        return in_array(
            PHP_SAPI,
            array(
                'cli',
                // 'cli-server'
            )
        );
    }

    /**
     * Get Multi - 获取多个, 可以设置默认值
     * @param array $data array data
     * @param array $needKeys
     * $needKeys = [
     *     'name',
     *     'password',
     *     'status' => '1'
     * ]
     * @param bool|false $unsetKey
     * @return array
     */
    public static function getMulti(array &$data, array $needKeys=[], $unsetKey=false)
    {
        $needed = [];

        foreach ($needKeys as $key => $value) {
            if ( is_int($key) ) {
                $key = $value;
                $default = null;
            } else {
                $default = $value;
            }

            if (isset($data[$key])) {
                $values[$key] = $data[$key];

                if ($unsetKey) {
                    unset($data[$key]);
                }
            } else {
                $values[$key] = $default;
            }
        }

        return $needed;
    }


    /**
     * get value from an array
     * @param $arr
     * @param $key
     * @param null $default
     * @return mixed
     */
    public static function arrayGet($arr, $key, $default = null)
    {
        return isset($arr[$key]) ? $arr[$key]: $default;
    }

    /**
     * get value and unset it
     * @param $arr
     * @param $key
     * @param null $default
     * @return mixed
     */
    public static function arrayRemove($arr, $key, $default = null)
    {
        if (isset($arr[$key])) {
            $value = $arr[$key];
            unset($arr[$key]);

            return $value;
        }

        return $default;
    }

    /**
     * @param $old
     * @param array $new
     * @return array
     */
    public static function arrayMerge($old, array $new)
    {
        if (!$old || !is_array($old)) {
            return $new;
        }

        foreach($new as $key => $value) {
            if ( array_key_exists($key, $old) && is_array($value)) {
                $old[$key] = self::arrayMerge($old[$key], $new[$key]);
            } elseif (is_int($key)) {
                $old[] = $value;
            } else {
                $old[$key] = $value;
            }
        }

        return $old;
    }
}
