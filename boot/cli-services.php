<?php
/**
 * register service for console application
 * @var $app \App
 */

require __DIR__ . '/services.php';

//
// can register custom service.
//

$app->register('input', function () {
    return new \app\components\console\Input();
}, true);

$app->register('output', function () {
    return new \app\components\console\Output();
}, true);