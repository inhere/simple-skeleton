<?php
/**
 * Created by PhpStorm.
 * User: inhere
 * Date: 2016/12/7
 * Time: 12:46
 */

use NoahBuscher\Macaw\Macaw;

Macaw::get('test',function(){
    echo 'hello, this is a test.';
});

Macaw::get('/', app\commands\Home::class . '@index');
Macaw::get('home(/[\w-]+)?', \app\commands\Home::class . '@distribute');
Macaw::get('image(/[\w-]+)?', app\commands\Image::class . '@distribute');
