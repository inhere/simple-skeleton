<h2>Image serial table</h2>
<hr>
<table class="table table-hover table-condensed table-bordered table-striped">
    <?php foreach ($table as $index => $row): ?>
        <tr>
            <th>#<?= ++$index ?></th>
            <?php foreach ($row as $value): ?>
                <td><?= $value ?></td>
            <?php endforeach ?>
        </tr>
    <?php endforeach ?>
</table>