<?php

use NoahBuscher\Macaw\Macaw;

Macaw::get('test',function(){
    echo 'hello, this is a test.';
});

Macaw::get('/', app\controllers\Home::class . '@index');
Macaw::any('home(/[\w-]+)?', \app\controllers\Home::class . '@distribute');

Macaw::get('image', app\controllers\Image::class . '@index');
Macaw::post('image/pack', app\controllers\Image::class . '@addPackTask');
Macaw::get('image/sn-list', app\controllers\Image::class . '@snList');
Macaw::get('image/gen-sn', app\controllers\Image::class . '@genSerialNumber');
Macaw::get('image/gen-isn', app\controllers\Image::class . '@genImgSerialNumber');
Macaw::get('image/gen-isn-by-sn', app\controllers\Image::class . '@genImgSerialNumberBySN');
Macaw::get('image/serial-table', app\controllers\Image::class . '@showSerialTable');

Macaw::any('qiniu(/[\w-]+)?', \app\controllers\Qiniu::class . '@distribute');

