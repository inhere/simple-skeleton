<?php

namespace app\commands;

use app\components\Command;

/**
 * default command controller
 */
class Home extends Command
{
    /**
     * this is a command's description message
     * the second line text
     * @usage usage message
     * @example example text
     */
    public function index()
    {
        $this->write("hello, welcome!! this is " . __METHOD__);
    }

    /**
     * a example for use color text output on command
     * @usage ./bin/app home/outColor
     */
    public function outColor()
    {
        if ( !$this->out->supportColor() ) {
            $this->write('Current terminal is not support output color text.');

            return 0;
        }

        $styles = $this->out->getColor()->getStyleNames();
        $this->write('normal text');

        foreach ($styles as $style) {
            $this->out->write("<$style>$style style text</$style>");
        }

        $this->out->block("message text");

        return 0;
    }


    /**
     * a example for use arguments on command
     * @usage home/useArgs [arg1=val1 arg2=arg2]
     * @example ./bin/app home/useArgs name=test status=2
     */
    public function useArgs()
    {
        print_r(\App::input()->get());
    }

    public function test()
    {
        $this->out->write('etse <info>info</info> <success>info</success>');
        print_r($_SERVER);
    }

    public function notFound()
    {
        echo "ohh!! page not found.";
    }
}
