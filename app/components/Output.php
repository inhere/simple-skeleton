<?php

namespace app\components;

use App;

/**
 *
 */
class Output
{
    const DEFAULT_CHARSET = 'UTF-8';

    /**
     * output charset
     * @var string
     */
    protected $charset = 'UTF-8';

    private $bodys = [];

    public function __construct()
    {
        $this->charset = App::$me->config('charset', self::DEFAULT_CHARSET);
    }

    public function write($content)
    {
        $this->bodys[] = $content;

        return $this;
    }

    /**
     * send response
     * @param  string $content
     * @return mixed
     */
    public function send($content = '')
    {
        if ($content) {
            $this->write($content);
        }

        header('Content-type: text/html; charset=' . $this->charset);

        $content = implode('', $this->bodys);

        if ( !$content && APP_DEBUG ) {
            throw new \RuntimeException('No content to display.');
        }

        \App::logger()->debug('Send text content.');

        echo $content;

        $this->bodys = [];

        return true;
    }

    /**
     * output json response
     * @param  array  $data
     * @return mixed
     */
    public function json(array $data)
    {
        header('Content-type: application/json; charset='.$this->charset);

        echo json_encode($data);

        return true;
    }

    public function formatJson($data, $code = 0, $msg = '')
    {
        // if `$data` is integer, queals to `formatJson(int $code, string $msg )`
        if ( is_numeric($data) ) {
            $jsonData = [
                'code'     => (int)$data,
                'msg'      => $code,
                'data'     => [],
            ];
        } else {
            $jsonData = [
                'code'     => (int)$code,
                'msg'      => $msg ?: 'successful!',
                'data'     => (array)$data,
            ];
        }

        return $this->json($jsonData);
    }

    /**
     * @param int $status
     * @param string $msg
     * @param array $data
     * @return mixed
     */
    public function formatJson2($status = 200, $msg = '', $data = [])
    {
        $jsonData = [
            'status'  => (int)$status,
            'message' => $msg ?: 'successful!',
            'data'    => $data,
        ];

        return $this->json($jsonData);
    }
}
