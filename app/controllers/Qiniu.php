<?php

namespace app\controllers;

use App;
use app\components\Controller;
use app\components\PublicHelper;
use Qiniu\Auth;

/**
 *
 */
class Qiniu extends Controller
{

    public function index()
    {
        $content = "hello, welcome!!";

        // vd('$_REQUEST :', $_REQUEST, '$_GET :', $_GET);
        $this->renderBody($content);
    }

    /**
     * 生成七牛上传token
     * @api /qiniu/up-token
     * @link http://developer.qiniu.com/code/v7/sdk/php.html
     * @return mixed
     */
    public function upToken()
    {
        $accessKey = App::$me->config('qiniu.accessKey');
        $secretKey = App::$me->config('qiniu.secretKey');
        $auth = new Auth($accessKey, $secretKey);

        $bucket = App::$me->config('qiniuBuckets.default');

        if ( !($bucket = App::input()->get('bucket', $bucket)) ) {
            return App::$me->output->formatJson('Require parameter [bucket].');
        }

        $upToken = $auth->uploadToken($bucket);

        return App::$me->output->json([
            'token' => $upToken
        ]);
    }
}
