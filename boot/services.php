<?php
/**
 * register service for web application
 * @var $app \App
 */

// add service
$app->register('pdo', function(\App $app)
{
    $options = array_merge([
        'dsn'  => 'mysql:host=localhost;port=3306;dbname=db_name;charset=UTF8',
        'user' => 'root',
        'pass' => '',
    ], $app->config('db',[]));

    // mysql:host=localhost;port=3333;dbname=DB;charset=UTF8
    try {
        $pdo = new \PDO($options['dsn'], $options['user'], $options['pass'] );
    } catch (\PDOException $e) {
        throw new \RuntimeException('Could not connect to DB: ' . $e->getMessage() . '. DSN: ' . $options['dsn']);
    }

    $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, true);

    return $pdo;
});

$app->register('view', function(\App $app)
{
    $options = $app->config('view', []);
    $options['debug'] = $app->isDebug();
    $viewsPath = PROJECT_PATH . '/' . $options['viewsPath'];
    $layout = isset($options['layout']) ? $options['layout'] : '';
    $attributes = isset($options['attributes']) ? $options['attributes'] : [];

    return new \app\components\SimpleRenderer($viewsPath, $layout, $attributes);
});

$app->register('taskLog', function(\App $app)
{
    $options = $app->config('taskLog', []);
    $options['debug'] = $app->isDebug();
    $options['basePath'] = PROJECT_PATH . '/' . $options['basePath'];

    return \app\components\SFLogger::make($options);
});

$app->register('logger', function(\App $app)
{
    $options = $app->config('log', []);
    $options['debug'] = $app->isDebug();
    $options['basePath'] = PROJECT_PATH . '/' . $options['basePath'];

    return \app\components\SFLogger::make($options);
});

$app->register('input', function(\App $app)
{
    return new \app\components\Input;
});

$app->register('output', function(\App $app)
{
    return new \app\components\Output;
});

$app->register('onNotFound', function()
{
    $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $method = $_SERVER['REQUEST_METHOD'];

    \App::logger()->warning("Page not found: $uri");

    $msg = " Request page not found! \n";

    if ( APP_DEBUG ) {
        $msg .= "\n . Maybe you not config route [$uri] in router file.";
        $msg .= "\n . Maybe you request method [$method] don't matched.";
        $msg .= "\n . Maybe you request action name don't exist.";
        $msg .= "\n . Maybe ... ... :)";
    }

    echo \App::$me->input->isWeb() ? str_replace("\n", '<br>', $msg) : $msg;
});

// can also use property add service.
$app->onBeforeRun = function(\App $app)
{
    $app->logger->debug('App run start. Time:' . microtime(true));
};
$app->register('onAfterRun', function(\App $app)
{
    $app->logger->debug('App run end. Time:' . microtime(true));
});
$app->onAppStop = function(\App $app)
{
    $app->logger->debug('App stopped. Time:' . microtime(true));

    \app\components\SFLogger::flushAll();
};
