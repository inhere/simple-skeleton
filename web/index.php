<?php
if (PHP_SAPI === 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $file = __DIR__ . $_SERVER['REQUEST_URI'];

    if (is_file($file)) {
        return false;
    }

    // fixed bug : 'PHP_SELF' => '/index.php/path'
    $_SERVER['PHP_SELF'] = '/index.php';
}

define('PROJECT_PATH', dirname(__DIR__));
define('VIEW_PATH', dirname(__DIR__) . '/app/views');

// fixed: PHP_SELF = 'index.php', it is should be '/index.php'
if (isset($_SERVER['PHP_SELF'])) {
    $_SERVER['PHP_SELF'] = '/' . ltrim($_SERVER['PHP_SELF'],'/ ');
}

require dirname(__DIR__) . '/vendor/autoload.php';

define('APP_ENV', \App::env('env', \App::ENV_PDT));

// create app instance
$app = new \App(dirname(__DIR__) . '/config/config.'.APP_ENV.'.ini');

require dirname(__DIR__) . '/boot/routes.php';

require dirname(__DIR__) . '/boot/services.php';

// run
$app->run();
