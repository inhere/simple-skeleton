<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ugirls</title>
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <style type="text/css">
        body {
            padding-top: 70px;
        }
        .table th{
            background-color: #eee;
        }
        .table-hover>tbody>tr:hover {
            /*font-weight:600;*/
            color:#2780e3;
        }
    </style>
</head>
<body>
    <header>
      <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container"> <!-- -fluid -->
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">UG</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li><a href="/">
                  <span class="glyphicon glyphicon-home"></span>
                  Home <span class="sr-only">(current)</span></a>
              </li>
            </ul>
            <p class="navbar-text">
                Hello, Welcome to here!
            </p>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
    </header>
    <div class="container-fluid">
        {_CONTENT_}
    </div>
</body>
</html>
