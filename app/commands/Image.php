<?php
/**
 * Created by PhpStorm.
 * User: inhere
 * Date: 2016/12/7
 * Time: 12:42
 */

namespace app\commands;

use App;
use app\components\Command;
use app\components\ImageSerial;

/**
 * @package app\commands
 */
class Image extends Command
{
    const WAIT_SEC = 3;

    /**
     *
     * check env info
     *
     * sdf sdf
     *
     */
    public function check()
    {
        echo 'function [exec] is available? ' . (function_exists('exec') ? 'true' : 'false' ) . PHP_EOL;

        echo "database config:\n";
        foreach (App::$me->config('db', []) as $name => $value) {
            echo "  $name $value\n";
        }

        echo '... complete!';
    }

    /**
     * run image pack task
     *
     * @usage ./bin/app image/packTask
     */
    public function packTask()
    {
        $table = ImageSerial::TABLE_NAME;
        $sql = "SELECT iUserId,iProductId,sISN FROM $table WHERE iStatus = 0 ORDER BY iCreated ASC LIMIT 1";

        App::logger()->debug('Run SQL: '. $sql);
        vd(App::$me);

        $hasTask = true;
        $stmt = App::pdo()->prepare($sql);

        do {
            $stmt->execute();

            if ( !$rst = $stmt->fetch(\PDO::FETCH_NUM) ) {
                sleep(self::WAIT_SEC);
//                $hasTask = false;
            } else {
                list($userId, $productId, $isn) = $rst;

//                exec("");

                App::taskLog()->info("Complete task: userId $userId, productId $productId");
            }

//            App::taskLog()->error("userId: $userId, productId: $productId, ISN: $isn");
        } while ($hasTask);
    }

}