<?php

namespace app\controllers;

use App;
use app\components\Controller;

/**
 *
 */
class Home extends Controller
{

    public function index()
    {
        $content = "hello, welcome!! this is " . __METHOD__;

        // vd('$_REQUEST :', $_REQUEST, '$_GET :', $_GET);
        $this->renderBody($content);
    }

    public function test()
    {
        vd(App::$me->input->get());
    }

    public function env()
    {
        App::$me->output->formatJson([
            'phpVersion' => PHP_VERSION,
            'env' => App::$me->config('env'),
            'debug' => App::$me->isDebug(),
        ]);
    }

    public function json()
    {
        App::logger()->trace('test info');

        App::$me->output->json([
            'code' => 0,
            'msg' => 'successful!',
            'data' => [
                'name' => 'value',
            ]
        ]);
    }

    public function notFound()
    {
        echo "ohh!! page not found.";
    }
}
