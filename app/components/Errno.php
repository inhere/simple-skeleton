<?php

namespace app\components;

/**
 * 错误编号
 *
 * @uses      Errno
 * @version   2016年10月26日
 * @author    lilin <lilin@ugirls.com>
 * @copyright Copyright 2010-2016 北京尤果网文化传媒有限公司
 * @license PHP Version 5.x {@link http://www.php.net/license/3_0.txt}
 */
class Errno
{
    const USER_NOT_LOGIN           = 420;  //未登陆
    const PARAMETER_ERROR          = 401;  //参数验证错误
    const PRIVILEGE_NOT_PASS       = 403;  //权限不符合
    const SIGN_ERROR               = 406;  //签名错误
    const HTTP_ERROR               = 407;  //http错误
    const SERVER_ERROR             = 500;  //内部错误
}
